var tope = Math.floor(Math.random() * 100)

var nodoNumeroMaximo = document.getElementById('numeroMaximo')
var nodoTextoNumeroMaximo = document.createTextNode(`Número máximo generado: ${tope}`)
nodoNumeroMaximo.appendChild(nodoTextoNumeroMaximo)

var nodoListaNumeros = document.getElementById('listaNumeros')
var nodoTextoNumero = null
var textoNumero = ''
for (var i = 0; i <= tope; i++) {
    textoNumero = `${i}` + ((i < tope) ? ' ' : '')
    nodoTextoNumero = document.createTextNode(textoNumero)
    nodoListaNumeros.appendChild(nodoTextoNumero)
}